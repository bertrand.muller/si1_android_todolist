package fr.muller_ledoux.todolist;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.sql.Date;
import java.util.Calendar;


public class AddItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
        dp.setMinDate(System.currentTimeMillis() - 1000);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addItem(View view) {

        EditText itemForm = ((EditText) findViewById(R.id.itemLabel));
        DatePicker dp = (DatePicker) findViewById(R.id.datePicker);

        Calendar calendar = Calendar.getInstance();
        calendar.set(dp.getYear(), dp.getMonth(), dp.getDayOfMonth(), 0,0,0);
        long timeMilli = calendar.getTimeInMillis();

        Date d = new Date(timeMilli);
        Item item = new Item(itemForm.getText().toString(),d);
        item.save(this);
        setResult(RESULT_OK);
        finish();
    }
}
