package fr.muller_ledoux.todolist;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;

public class TriActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener  {

    private final static String pref1 = "mode_tri";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tri);

        Switch s = (Switch) findViewById(R.id.switch1);
        s.setOnCheckedChangeListener(this);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean sorted = preferences.getBoolean("sort", false);

        s.setChecked(sorted);




    }

    public void validerTri(View view) {

        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);

        if(rg.getVisibility() == View.VISIBLE) {


            RadioButton radioSelected = (RadioButton) findViewById(rg.getCheckedRadioButtonId());

            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putString(pref1, (String) radioSelected.getTag());
            editor.apply();


        }


        this.finish();


    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean("sort", isChecked);
        editor.apply();

        if (isChecked) {
            rg.setVisibility(View.VISIBLE);


            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            String modeTri = preferences.getString(pref1, "");

            if(modeTri.equals("ASC")){

                RadioButton rb = (RadioButton) findViewById(R.id.radioButton2);
                rb.setChecked(true);

            } else {

                RadioButton rb = (RadioButton) findViewById(R.id.radioButton);
                rb.setChecked(true);


            }

        } else {
            rg.setVisibility(View.INVISIBLE);
        }

    }
}
