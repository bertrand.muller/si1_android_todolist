package fr.muller_ledoux.todolist;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Paint;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, AdapterView.OnItemClickListener {

    private static final int ACT_ADD_ITEM = 1;
    private static final String SORTED = "sort";
    private SimpleCursorAdapter dataAdapter;
    private ListView lv;
    private boolean sorted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = ((ListView) findViewById(R.id.todolist));
        lv.setOnItemLongClickListener(this);
        lv.setOnItemClickListener(this);

        FloatingActionButton myFab = (FloatingActionButton) findViewById(R.id.fab);

        myFab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent addItem = new Intent(MainActivity.this, AddItemActivity.class);
                startActivityForResult(addItem, ACT_ADD_ITEM);

            }
        });

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        sorted = preferences.getBoolean(SORTED, false);

        displayItems(true);

    }


    @Override
    public void onResume(){

        super.onResume();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        sorted = preferences.getBoolean(SORTED, false);

        System.out.println("*************************");
        displayItems(true);
        System.out.println("*************************");

    }


    private void displayItems(final boolean afficherNotif) {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String modeTri =  preferences.getString("mode_tri", "");

        final Cursor cursor;
        cursor = TodoBase.fetchAllItems(this, sorted, modeTri);

        System.out.println(afficherNotif);

        dataAdapter = new SimpleCursorAdapter(this, R.layout.row, cursor,
                                              new String [] {TodoBase.KEY_LABEL},
                                              new int [] {R.id.label},
                                              0) {

            @Override
            public void setViewText(TextView tv, String text) {

                System.out.println(afficherNotif);

                Date dateCour = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                String label = cursor.getString(cursor.getColumnIndex("label"));
                int columnIndex = cursor.getColumnIndex("dateExp");
                Date dateExp = null;

                try {
                    dateExp = dateFormat.parse(cursor.getString(columnIndex));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if ((dateExp != null) && (dateExp.compareTo(dateCour) < 0)) {
                    tv.setTextColor(Color.RED);
                    tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    System.out.println("Rouge : " + label + " / " + dateExp.toString());
                } else {
                    tv.setTextColor(Color.BLACK);
                    System.out.println("Noir : " + label + " / " + dateExp.toString());
                }
/*
                System.out.println(nbRouges[0]);

                if(nbRouges[0] > 0 && afficherNotif) {

                    NotificationCompat.Builder mBuilder =
                            new NotificationCompat.Builder(MainActivity.this)
                                    .setSmallIcon(R.mipmap.web_hi_res_512)
                                    .setContentTitle("Liste")
                                    .setContentText("Vous avez " + nbRouges[0]/4 + " item(s) expiré(s) !");

                    int mNotificationId = 1;
                    // Gets an instance of the NotificationManager service
                    NotificationManager mNotifyMgr =
                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    // Builds the notification and issues it.
                    mNotifyMgr.notify(mNotificationId, mBuilder.build());

                }*/

                super.setViewText(tv, text);
                cursor.moveToNext();


            }

        };

        lv.setAdapter(dataAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.menu_sort);
        item.setTitle((sorted) ? R.string.not_sort : R.string.sort);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.db_debug) {
            Intent dbManager = new Intent(this,AndroidDatabaseManager.class);
            startActivity(dbManager);
        }

        if (id == R.id.add_item) {
            Intent addItem = new Intent(this, AddItemActivity.class);
            startActivityForResult(addItem, ACT_ADD_ITEM);
        }

        if (id == R.id.menu_sort) {
            sorted = !sorted;

            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
            editor.putBoolean(SORTED, sorted);
            editor.apply();

            System.out.println(sorted);

            displayItems(false);
        }

        if (id == R.id.menu_tri) {

            Intent triSelect = new Intent(this, TriActivity.class);
            startActivityForResult(triSelect, ACT_ADD_ITEM);

        }

        if(id == R.id.menu_del){

            SQLiteDatabase bdd = TodoBase.getDB(this);
            bdd.execSQL("DELETE FROM items");
            bdd.close();
            displayItems(false);

        }

        return super.onOptionsItemSelected(item);
    }



    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACT_ADD_ITEM:
                // Cursor is not dynamic, we have to fetch data again
                // Not optimal, but a dynamic cursor requires a CursorLoader,
                // which requires a ContentProvider... More work, more difficult
                displayItems(false);
                break;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        editor.putBoolean(SORTED, sorted);
        editor.apply();
    }

    @Override
    public boolean onItemLongClick(final AdapterView<?> adapterView, View view, final int i, long l) {


        TextView txtv = (TextView) view.findViewById(R.id.label);
        String label = txtv.getText().toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder

                .setTitle("Suppression d'un item")
                .setMessage("Voulez-vous vraiment supprimer l'item '" + label + "' ?")

                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        TodoBase.deleteItem(MainActivity.this, (int) adapterView.getItemIdAtPosition(i));
                        MainActivity.this.displayItems(false);

                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .show();



        return false;
    }

    @Override
    public void onItemClick(final AdapterView<?> adapterView, final View view, final int position, long id) {

        TextView tv = (TextView) view.findViewById(R.id.label);
        String label = tv.getText().toString();
        LayoutInflater inflater = this.getLayoutInflater();
        View text_area =  inflater.inflate(R.layout.text_area, null);
        final EditText et = (EditText) text_area.findViewById(R.id.textArea);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder

                .setTitle("Modification d'un item")
                .setMessage("Entrez la nouvelle chaîne pour l'item '" + label + "' :")
                .setView(text_area)

                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        String text = et.getText().toString();

                        if (text.matches("[a-zA-Z0-9]+")) {

                            TodoBase.changeTextItem(MainActivity.this, (int) adapterView.getItemIdAtPosition(position), text);
                            MainActivity.this.displayItems(false);
                        } else {

                            //Affichage d'une nouvelle fenetre si chaine invalide
                            onItemClick(adapterView,view,position,id);

                        }

                    }

                })

                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }

                })

                .show();

    }

}
