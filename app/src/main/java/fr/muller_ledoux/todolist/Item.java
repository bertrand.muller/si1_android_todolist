package fr.muller_ledoux.todolist;

import java.sql.Date;

/**
 * Created by phil on 24/02/15.
 */
public class Item {
    private long id;
    private String label;
    private Date date;

    public Item(String s, Date d) {

        date = d;
        label = s;
        id = -1;
    }

    public Item(long id, String label, Date d) {
        this.id = id;
        this.date = d;
        this.label = label;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getDate(){

        return date;
    }

    public void setDate(Date d){

        date = d;
    }


    public void save(AddItemActivity addItemActivity) {
        if (id == -1)
            saveNewItem(addItemActivity);
    }

    private void saveNewItem(AddItemActivity addItemActivity) {
        TodoBase.addItem(addItemActivity, label, date);
    }
}
